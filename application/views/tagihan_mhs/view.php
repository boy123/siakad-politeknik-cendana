<div class="row">
	<div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-left bordered-darkorange">
                <span class="widget-caption">Keuangan Mahasiswa</span>
                <div class="widget-buttons">

                    
                </div>
            </div>
            <div class="widget-body bordered-left bordered-warning">

            	

                <div class="table-scrollable">
	                <table class="table table-bordered table-hover table-striped" id="searchable">
	                    <thead class="bordered-darkorange">
	                        <tr role="row">
	                            <th>No</th>
								<th>Nim</th>
								<th>Nama</th>
								<th>Prodi</th>
								<th>Option</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    <?php 
							$no = 1;
							foreach ($this->db->get('mahasiswa')->result() as $rw) {
						 ?>
	            
	                        <tr>
	                            <td><?php echo $no; ?></td>
	                            <td><?php echo $rw->nim; ?></td>
								<td><?php echo $rw->nama; ?></td>
								<td><?php echo get_data('prodi','id_prodi',$rw->id_prodi,'prodi') ?></td>
	                            
	                            <td>
	                            	<a href="tagihan_mhs/detail/<?php echo $rw->id_mahasiswa ?>" class="label label-info"><i class="fa fa-eye"></i> Detail</a>
	                            </td>
	                            
	                        </tr>

	                    <?php $no++; } ?>
	                        
	                    </tbody>
	                </table>
            	</div>


            </div>
        </div>
    </div>
</div>
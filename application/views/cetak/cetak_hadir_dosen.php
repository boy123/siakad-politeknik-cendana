<body onload="window.print()">
<!-- <body > -->
<!-- <a target="_blank" href="cetak/cetak_absen_kosong/" class="btn btn-primary">Cetak Absen</a> -->
</body>
<style type="text/css">
    body
    {
        font-family: sans-serif;
        font-size: 14px;
    }
    th{
        padding: 5px;
        font-weight: bold;
        font-size: 12px;
    }
    td{
        font-size: 12px;
        padding: 3px;
    }
    h2{
        text-align: left;
        margin-bottom: 13px;
    }
    .potong
    {
        page-break-after:always;
    }
</style>

<?php //$this->load->view('kop'); ?>
<?php 
    $id_prodi = $this->input->get('id_prodi');
    $kode_mk = $this->input->get('kode_mk');
    $kelas = $this->input->get('kelas');
    $kode_semester = $this->input->get('periode');
 ?>
<h3 align="center">DAFTAR HADIR DOSEN DAN BATAS KULIAH</h3>
<h3 align="center">SEMESTER GENAP TA. <?php echo get_data('tahun_akademik','kode_tahun',$kode_semester,'keterangan') ?></h3><br>

<table border="0" style="border-collapse: collapse;width: 100%;">
    <tr>
        <td style="width: 100px;">Nama</td><td align="left">: <?php echo strtoupper($this->session->userdata('nama'))?></td>
        <td style="width: 100px;">Prodi</td><td align="left">: <?php echo strtoupper(get_data('prodi','id_prodi',$id_prodi,'prodi')) ?></td>
    </tr>

    <tr>
        <td style="width: 100px;">Mata Kuliah</td><td align="left">: 
            <?php 
            $this->db->where('id_prodi', $id_prodi);
            $this->db->where('kode_mk', $kode_mk);
            echo strtoupper($this->db->get('matakuliah')->row()->nama_mk);
            ?></td>
        <td style="width: 100px;">Jumlah SKS</td><td align="left">: 
            <?php 
            $this->db->where('id_prodi', $id_prodi);
            $this->db->where('kode_mk', $kode_mk);
            echo $this->db->get('matakuliah')->row()->sks_total;
            ?></td>
    </tr>
    <tr>
        <td style="width: 100px;">Kode MK</td><td align="left">: <?php echo strtoupper($kode_mk)?></td>
        <td style="width: 100px;">Periode</td><td align="left">: <?php echo $kode_semester ?></td>
    </tr>

    
</table>
<br>
<table border="1" style="border-collapse: collapse;width: 100%;">

    <tr>
        <th  width="10">NO</th>
        <th >Tanggal</th>
        <th >Materi</th>
        <th >Metode</th>
        
        <th width="100" height="50">Tanda Tangan</th>
    </tr>
    
    <?php 
    for ($i=1; $i < 17 ; $i++) { 
     ?>
    <tr>
        <td><?php echo $i; ?></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <?php } ?>
</table>


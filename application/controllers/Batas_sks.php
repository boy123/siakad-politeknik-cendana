<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Batas_sks extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Batas_sks_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'batas_sks/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'batas_sks/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'batas_sks/index.html';
            $config['first_url'] = base_url() . 'batas_sks/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Batas_sks_model->total_rows($q);
        $batas_sks = $this->Batas_sks_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'batas_sks_data' => $batas_sks,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'Set Batas SKS',
            'konten' => 'batas_sks/batas_sks_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Batas_sks_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_batas_krs' => $row->id_batas_krs,
		'ipk' => $row->ipk,
		'batas_sks' => $row->batas_sks,
	    );
            $this->load->view('batas_sks/batas_sks_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('batas_sks'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'Set Batas SKS',
            'konten' => 'batas_sks/batas_sks_form',
            'button' => 'Simpan',
            'action' => site_url('batas_sks/create_action'),
	    'id_batas_krs' => set_value('id_batas_krs'),
	    'ipk' => set_value('ipk'),
	    'batas_sks' => set_value('batas_sks'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'ipk' => $this->input->post('ipk',TRUE),
		'batas_sks' => $this->input->post('batas_sks',TRUE),
	    );

            $this->Batas_sks_model->insert($data);
            $this->session->set_flashdata('message', '<div class="alert alert-success fade in alert-radius-bordered alert-shadowed">
                                        <button class="close" data-dismiss="alert">
                                            ×
                                        </button>
                                        <i class="fa-fw fa fa-info"></i>

                                        <strong>Info:</strong> Data Berhasil disimpan
                                    </div>');
            redirect(site_url('batas_sks'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Batas_sks_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'Set Batas SKS',
                'konten' => 'batas_sks/batas_sks_form',
                'button' => 'Ubah',
                'action' => site_url('batas_sks/update_action'),
		'id_batas_krs' => set_value('id_batas_krs', $row->id_batas_krs),
		'ipk' => set_value('ipk', $row->ipk),
		'batas_sks' => set_value('batas_sks', $row->batas_sks),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('batas_sks'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_batas_krs', TRUE));
        } else {
            $data = array(
		'ipk' => $this->input->post('ipk',TRUE),
		'batas_sks' => $this->input->post('batas_sks',TRUE),
	    );

            $this->Batas_sks_model->update($this->input->post('id_batas_krs', TRUE), $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success fade in alert-radius-bordered alert-shadowed">
                                        <button class="close" data-dismiss="alert">
                                            ×
                                        </button>
                                        <i class="fa-fw fa fa-info"></i>

                                        <strong>Info:</strong> Data Berhasil diubah
                                    </div>');
            redirect(site_url('batas_sks'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Batas_sks_model->get_by_id($id);

        if ($row) {
            $this->Batas_sks_model->delete($id);
            $this->session->set_flashdata('message', '<div class="alert alert-success fade in alert-radius-bordered alert-shadowed">
                                        <button class="close" data-dismiss="alert">
                                            ×
                                        </button>
                                        <i class="fa-fw fa fa-info"></i>

                                        <strong>Info:</strong> Data Berhasil dihapus
                                    </div>');
            redirect(site_url('batas_sks'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('batas_sks'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('ipk', 'ipk', 'trim|required');
	$this->form_validation->set_rules('batas_sks', 'batas sks', 'trim|required');

	$this->form_validation->set_rules('id_batas_krs', 'id_batas_krs', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Batas_sks.php */
/* Location: ./application/controllers/Batas_sks.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2021-02-07 15:05:37 */
/* https://jualkoding.com */
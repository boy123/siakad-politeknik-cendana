<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tagihan_mhs extends CI_Controller {

	public function index()
	{
		$data = array(
			'konten' => 'tagihan_mhs/view',
			'judul_page' => 'Keuangan Mahasiswa',
		);
		$this->load->view('v_index',$data);
	}

	public function detail($id_mahasiswa)
	{
		$data = array(
			'd_mhs' => $this->db->get_where('mahasiswa', ['id_mahasiswa'=>$id_mahasiswa]),
			'konten' => 'tagihan_mhs/detail',
			'judul_page' => 'Keuangan Mahasiswa',
		);
		$this->load->view('v_index',$data);
	}

	public function simpan_tagihan($id_mahasiswa)
	{
		$this->db->insert('tagihan_mahasiswa', $_POST);
		redirect('tagihan_mhs/detail/'.$id_mahasiswa,'refresh');
	}

	public function lunas($status, $id_tagihan)
	{
		$id_mahasiswa = get_data('tagihan_mahasiswa','id_tagihan',$id_tagihan,'id_mahasiswa');
		$this->db->where('id_tagihan', $id_tagihan);
		$this->db->update('tagihan_mahasiswa', array('status' => $status));
		redirect('tagihan_mhs/detail/'.$id_mahasiswa,'refresh');
	}

}

/* End of file Tagihan_mhs.php */
/* Location: ./application/controllers/Tagihan_mhs.php */